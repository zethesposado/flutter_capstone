# Flutter Capstone App

Read the details about the Flutter Capstone App on this document. App features, versions, packages, and guide on how to clone this project.

# Preview of Application

![Capture1.JPG](readme-images/Capture1.jpg)

![Capture2.JPG](readme-images/Capture2.jpg)

![Capture3.JPG](readme-images/Capture3.jpg)

![Capture4.JPG](readme-images/Capture4.jpg)

# About the App

Project Management app that can be used by contractors, subcontractors, and assembly-team, for easy managing and monitoring of projects and tasks under it.


# Setup Guide

You can use the terminal, [SourceTree](https://confluence.atlassian.com/get-started-with-sourcetree/clone-a-remote-repository-847359098.html), or any other client you'd like to clone your Git repository. These instructions show you how to clone your repository using Git from the terminal.

1. From the repository, click **+** in the global sidebar and select **Clone this repository** under **Get to work**.
2. Copy the clone command.
3. From a terminal window, change into the local directory where you want to clone your repository.

    `$ cd <path_to_directory>` 

4. Paste the command you copied from Bitbucket, for example:

    `$ git clone https://username@bitbucket.org/teamsinspace/documentation-tests.git` 

If the clone was successful, a new sub-directory appears on your local drive with the same name as the repository that you cloned.

# Flutter version

```powershell
Flutter 2.2.3
```

# Packages

- [http ^0.13.3](https://pub.dev/packages/http) - A composable, Future-based library for making HTTP requests.
- [provider ^5.0.0](https://pub.dev/packages/provider/versions/5.0.0-nullsafety.5) - A wrapper around InheritedWidget to make them easier to use and more reusable.
- [shared_preferences ^2.0.6](https://pub.dev/packages/shared_preferences) - Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.)
- [image_picker ^0.8.0+3](https://pub.dev/packages/image_picker) - A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.
- [flutter_dotenv ^5.0.0](https://pub.dev/packages/flutter_dotenv) - Easily configure any flutter application with global variables using a `.env` file.

# Features of the App

## Contractor

- Login and view Project List
- Add a new project and assign it to the subcontractor
- Add a task to the selected project and assign the task to the assembly team
- Manage and review finished project tasks.

## Subcontractor

- Login and view assigned projects
- View project tasks
- Add and assign a task to assembly team

## Assembly Team

- View projects with assigned tasks
- Mark task as ongoing and completed


### Test User Credentials
| Email   |      Password      | 
|----------|----------|
| contractor@gmail.com | contractor | 
| subcontractor@gmail.com | subcontractor | 
| assembly-team@gmail.com | assembly-team | 

# Getting Packages

Open `pubspec.yaml` and add packages under dependencies

```dart
dependencies:
  http: ^0.13.3
  provider: ^5.0.0
  shared_preferences: ^2.0.6
  image_picker: ^0.8.0+3
  flutter_dotenv: ^5.0.0
```

From the terminal, run the following commands:

```powershell
$ flutter pub get
```