import 'package:flutter/material.dart';

var btnDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.white, // Background color
    side: BorderSide(width: 1.0, color: Colors.black),
    onPrimary: Colors.black, // Text color
    padding: EdgeInsets.all(16.0)
);

var btnRedTheme = ElevatedButton.styleFrom(
    primary: Colors.red.shade900, // Background color
    onPrimary: Colors.white, // Text color
    padding: EdgeInsets.all(16.0)
);