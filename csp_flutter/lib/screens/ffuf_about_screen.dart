


import 'dart:ui';

import 'package:flutter/material.dart';

class AboutFFUFScreen extends StatefulWidget {
    @override
    AboutFFUFScreenState createState() => AboutFFUFScreenState();
}

class AboutFFUFScreenState extends State<AboutFFUFScreen> {

    @override
    Widget build(BuildContext context) {
        return Container(
            child: Stack(
                alignment: Alignment.center,
                children: [
                    Padding(
                        padding: EdgeInsets.all(16.0),
                            child: Align(
                                alignment: FractionalOffset.topCenter,
                                child: Image.asset('assets/ffuf-logo.png'),
                        ),
                    ),
                    Positioned(
                        child: Container(
                            height: 350.0,
                            width: 950.0,
                            margin: EdgeInsets.all(24.0),
                            child: Card(
                                color: Color.fromRGBO(20, 45, 68, 1),
                                elevation: 8.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0)
                                ),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                        Padding(
                                            padding:EdgeInsets.all(16.0),
                                            child: Text('About FFUF Manila Inc.', 
                                                style: TextStyle(fontSize: 24.0, 
                                                fontWeight: FontWeight.bold, color: Colors.white)), 
                                        ),
                                        Padding(
                                            padding: EdgeInsets.all(16.0),
                                            child: Text('FFUF Manila Inc is part of a chain of Digital-Service-Provider rooted in Germany responsible for delivering quality software components for Web and Mobile Platforms. We specialize in REST-full web services with amazing Angular applications, developing apps for laser measurement devices and autonomous mowing robots. Yes, robots! Gotta love the internet of things! We are strategists, project managers, designers, developers, engineers, analysts and most importantly, we are a family, wherever we are- in Germany and the Philippines.', style: TextStyle(color: Colors.white), textAlign: TextAlign.center, )
                                        ),
                                    ],
                                ),
                            ),
                        )
                    )
                ],
            ),
        );
    }
}