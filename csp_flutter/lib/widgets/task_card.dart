import 'package:csp_flutter/utils/api.dart';
import 'package:csp_flutter/widgets/add_task_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';
import '/utils/themes.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}
class _TaskCard extends State<TaskCard> {    
    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;

        final bool isTaskPending = widget._task.status == 'Pending';
        final bool isTaskOngoing = widget._task.status == 'Ongoing';
        final bool isTaskCompleted = widget._task.status == 'Completed';

        final bool isTaskRejected = widget._task.status == 'Rejected';
        final bool isTaskAccepted = widget._task.status == 'Accepted';

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // Modify both the main and cross axis alignment.
                        children: [
                            Text(widget._task.title, style: TextStyle(fontSize: 19.0)),
                            Text(widget._task.description)
                        ]
                    )
                ),
                Chip(label: Text(widget._task.status!))
            ]
        );

        Widget btnStart = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                style: btnRedTheme,
                child: Text('Start'),
                onPressed: () { 
                    setState(() {
                      API(accessToken).updateTaskStatus(
                          taskId: widget._task.id, 
                          status: 'Ongoing'
                        );
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnFinish = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                style: btnRedTheme,
                child: Text('Finish'),
                onPressed: () { 
                    setState(() {
                        API(accessToken).updateTaskStatus(
                            taskId: widget._task.id, 
                            status: 'Completed'
                        );
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnAccept = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                style: btnRedTheme,
                child: Text('Accept'),
                onPressed: () { 
                    setState(() {
                        API(accessToken).updateTaskStatus(
                            taskId: widget._task.id, 
                            status: 'Accepted'
                        );
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnReject = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                style: btnDefaultTheme,
                child: Text('Reject'),
                onPressed: () { 
                    setState(() {
                        API(accessToken).updateTaskStatus(
                            taskId: widget._task.id, 
                            status: 'Rejected'
                        );
                    });
                    widget._reloadTasks();
                },
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text('Detail', style: TextStyle(fontWeight: FontWeight.w900)),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),
                (designation == 'assembly-team' && isTaskPending) ? btnStart : Container(),
                // Show btnStart if designation is assembly team and task status is pending, else show an empty container.
                (designation == 'assembly-team' && isTaskOngoing) ? btnFinish : Container(),
                // Show btnFinish if designation is assembly team and task status is ongoing, else show an empty container.
                (designation == 'contractor' && (isTaskCompleted || isTaskRejected)) ? btnAccept : Container(), 
                // Show btnAccept if designation is contractor and task status is completed or rejected, else show an empty container.
                (designation == 'contractor' && (isTaskCompleted || isTaskAccepted)) ? btnReject : Container()
                // Show btnReject if designation is contractor and task status is completed or accepted, else show an empty container.
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}