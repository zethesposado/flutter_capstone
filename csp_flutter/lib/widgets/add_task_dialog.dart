import 'package:csp_flutter/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class AddTaskDialog extends StatefulWidget {
    final int? _projectId;
    
    AddTaskDialog( this._projectId );

    @override
    _AddTaskDialog createState() => _AddTaskDialog();
}

class _AddTaskDialog extends State<AddTaskDialog> {
    final _formKey = GlobalKey<FormState>();
    
    final _txtTitleController = TextEditingController();
    final _txtDescriptionController = TextEditingController();

    List<DropdownMenuItem> _assemblyTeamOptions = [];
    int? _assignedTo;

    void addTask(BuildContext context) {
        final String? accesToken = Provider.of<UserProvider>(context, listen: false).accessToken;

        API(accesToken).addTask(
            title: _txtTitleController.text,
            description: _txtDescriptionController.text,
            assignedTo: _assignedTo, 
            projectId: widget._projectId
        ).catchError((error) {
            showSnackBar(context, error.message);
        });
    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            API(accessToken).getUsersByDesignation('assembly-teams').then((resolvers) {
                setState(() {
                    _assemblyTeamOptions = resolvers.map((resolver) {
                        return DropdownMenuItem(
                            child: Text(resolver.email!),
                            value: resolver.id
                        );
                    }).toList();
                });
            }).catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);

        Widget txtTitle = TextFormField(
            decoration: InputDecoration(labelText: 'Title'),
            keyboardType: TextInputType.text,
            controller: _txtTitleController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Title is required.';
            }
        );

        Widget txtDescription = TextFormField(
            decoration: InputDecoration(labelText: 'Description'),
            keyboardType: TextInputType.text,
            controller: _txtDescriptionController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Description is required.';
            }
        );

        DropdownButtonFormField txtAssemblyTeam = DropdownButtonFormField(
            decoration: InputDecoration(labelText: 'Assembly Team'),
            items: _assemblyTeamOptions,
            onChanged: (value) {
                setState(() {
                _assignedTo = value;
                });
                // Update the assignedTo state according to selected option.
            }
        );

        Widget formAddTask = Form(
            key: _formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    txtTitle,
                    txtDescription,
                    txtAssemblyTeam
                ]
            )
        );

        return AlertDialog(
            title: Text('Add New Task', style: TextStyle(fontWeight: FontWeight.w700),),
            content: Container(
                child: SingleChildScrollView(
                    child: formAddTask
                )
            ),
            actions: [
                ElevatedButton(
                    style: btnRedTheme,
                    child: Text('Add'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            addTask(context);
                            Navigator.of(context).pop();
                        } else {
                            showSnackBar(context, 'Form validation failed. Check input and try again.');
                        }
                    }
                ),
                ElevatedButton(
                    style: btnRedTheme,
                    child: Text('Cancel'),
                    onPressed: () {
                        Navigator.of(context).pop();
                    }
                ),
            ],
        );
    }
}